<div class="form-group{{ $errors->has('userid') ? ' has-error' : '' }}">
  {!! Form::label('userid', 'User ID', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('userid', $userid, ['class'=>'form-control']) !!}
    {!! $errors->first('userid', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group">
  <div class="col-md-4 col-md-offset-2">
    {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!} 
  </div>
</div>

