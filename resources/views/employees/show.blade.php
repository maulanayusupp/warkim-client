@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">View Employee</div>

                <div class="panel-body">
                    @if (session()->has('success'))
                        <div class="alert alert-success" role="alert">{{ session('success') }}</div>
                    @endif

                    {!! Form::open(['url' => url('/show'), 
                    'method' => 'get', 'class'=>'form-horizontal']) !!}
                    @include('employees._userid')
                    {!! Form::close() !!}

                @if (is_object($employee))

                    {!! Form::model($employee, ['url' => url('/employees', $employee->userid), 
                      'method'=>'put', 'class'=>'form-horizontal']) !!}
                        @include('employees._form')
                    {!! Form::close() !!}

                    {!! Form::model($employee, ['url' => url('/employees', $employee->userid), 'method' => 'delete', 'class' => 'form-horizontal'] ) !!}
                        <div class="form-group">
                          <div class="col-md-4 col-md-offset-2">
                            {!! Form::submit('Hapus', ['class'=>'btn btn-danger']) !!}
                          </div>
                        </div>
                    {!! Form::close()!!}
                @elseif (request()->has('userid'))
                    Tidak ada data ditemukan
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


