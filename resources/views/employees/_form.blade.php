<div class="form-group{{ $errors->has('nama_lengkap') ? ' has-error' : '' }}">
  {!! Form::label('nama_lengkap', 'Nama', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('nama_lengkap', null, ['class'=>'form-control']) !!}
    {!! $errors->first('nama_lengkap', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('nip') ? ' has-error' : '' }}">
  {!! Form::label('nip', 'NIP', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('nip', null, ['class'=>'form-control']) !!}
    {!! $errors->first('nip', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email', 'Alamat Email', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-4">
        {!! Form::email('email', null, ['class'=>'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
  <div class="col-md-4 col-md-offset-2">
    {!! Form::submit(isset($employee) ? 'Update' : 'Save', ['class'=>'btn btn-primary']) !!} 
  </div>
</div>

