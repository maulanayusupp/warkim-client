<div class="form-group{{ $errors->has('q') ? ' has-error' : '' }}">
  {!! Form::label('q', 'Nama / Email', ['class'=>'col-md-6 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('q', $q, ['class'=>'form-control']) !!}
    {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group">
  <div class="col-md-4 col-md-offset-4">
    {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!} 
  </div>
</div>

