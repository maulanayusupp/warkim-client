@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Create Employee</div>

                <div class="panel-body">
                {!! Form::open(['url' => url('/employees'), 
                  'method' => 'post', 'files'=>'true', 'class'=>'form-horizontal']) !!}
                  @include('employees._form')
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


