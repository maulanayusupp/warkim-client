<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\MessageBag;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = '';
        $client = new Client(['base_uri' => config('api.url')]);
        $uri = '/employees';
        if ($request->has('q')) {
            $q = $request->get('q');
            $uri = '/employees?q=' . $q;
        }
        $res = $client->request('GET', $uri);
        $result = $res->getBody();
        return view('employees.list', compact('result', 'q'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_lengkap' => 'required',
            'nip' => 'required',
            'email' => 'required|email'
        ]);

        $client = new Client(['base_uri' => config('api.url')]);
        $uri = '/employees';
        $form = [ 'form_params' => $request->toArray() ];
        try {
            $res = $client->request('POST', $uri, $form);
            $employee = json_decode($res->getBody());
            return redirect('show?userid=' . $employee->userid);
        } catch (ClientException $e) {
            // handle server validation
            if ($e->hasResponse()) {
                $res = $e->getResponse();
                if ($res->getStatusCode() == 422) {
                    $messages = json_decode($res->getBody(), true);
                    $errors = new MessageBag($messages);
                    return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
                }
            }
            // unknown error
            abort(404, 'Unknown server error.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $userid = '';
        $employee = [];
        if ($request->has('userid')) {
            $client = new Client(['base_uri' => config('api.url')]);
            $userid = $request->get('userid');
            $endpoint = '/employees/' . $userid;
            $res = $client->request('GET', $endpoint);
            if ($res->getStatusCode() == 200) {
                $employee = json_decode($res->getBody());
            }
        }
        return view('employees.show', compact('userid', 'employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_lengkap' => 'required',
            'nip' => 'required',
            'email' => 'required|email'
        ]);

        $client = new Client(['base_uri' => config('api.url')]);
        $uri = '/employees/' . $id;
        $form = [ 'form_params' => $request->toArray() ];
        try {
            $res = $client->request('PUT', $uri, $form);
            return redirect('show?userid=' . $id);
        } catch (ClientException $e) {
            // handle server validation
            if ($e->hasResponse()) {
                $res = $e->getResponse();
                if ($res->getStatusCode() == 422) {
                    $messages = json_decode($res->getBody(), true);
                    $errors = new MessageBag($messages);
                    return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
                }
            }
            // unknown error
            abort(404, 'Unknown server error.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = new Client(['base_uri' => config('api.url')]);
        $uri = '/employees/' . $id;
        try {
            $res = $client->request('DELETE', $uri);
            session()->flash('success', 'Berhasil!');
            return redirect('show');
        } catch (ClientException $e) {
            // unknown error
            abort(404, 'Unknown server error.');
        }
    }
}
