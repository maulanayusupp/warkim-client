# Cara Menjalankan
1. Download
2. Instal dengan perintah `composer install`
3. Set key dengan `php artisan key:generate`
4. Jalankan dengan `php artisan serve --port=8001` (Port 8000 sudah digunakan server)

# File yang perlu diperhatikan
- `app/Http/routes.php` (semua endpoint ada disini)
- `app/Http/Controllers/EmployeesController.php`
- `resources/views/employees/*` (semua view)
- `config/api.php` (set alamat server)

